# README #
***

This is an example of Convaw's Game of Life at PHP (https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

There is no need for use some framework.

This example shows analytics, OOP, PHP, programming and English skills.

For this example is index shortened (functions, css, php, html). In real situation it will be in a separated files.

# Result #
***

![animation.gif](https://bitbucket.org/repo/aaRK7q/images/4063664573-animation.gif)